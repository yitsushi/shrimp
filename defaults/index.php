<?php

define('DEBUG',    true);

if (DEBUG) {
  error_reporting(-1);
  ini_set('display_errors', '1');
}

define('DS', DIRECTORY_SEPARATOR);

define('ROOT',    dirname(__FILE__));

require_once(ROOT . DS . "lib" . DS . "shrimp" . DS . "bootstrap.php");

/*
Debug::start();
?>
PHP_ENV:          <?= PHP_ENV ?> --
Author:           <?= Config::get('page.author') ?> --
Title:            <?= Config::get('page.title') ?> --
Extension:        <?= Request::extension() ?> --
Controller:       <?= Request::controller() ?> --
Action:           <?= Request::action() ?> --
<?php
Debug::end();
*/
