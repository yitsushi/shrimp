Shrimp - Cut For You...
=======================

Requirements
------------

* mod_rewrite (or similar but maybe you need to port .htaccess file)
* PHP 5.3+
* git (http://git-scm.com/)

Install
-------

    $ cd path/to/your/webroot
    $ mkdir lib
    $ git clone git@bitbucket.org:yitsushi/shrimp.git lib/shrimp
    $ cp lib/shrimp/defaults/_htaccess .htaccess
    $ cp lib/shrimp/defaults/index.php index.php
    $ cp lib/shrimp/defaults/static/shrimp.png shrimp.png
    $ mkdir cache
    $ chmod 777 cache

**To add an example plugin**

    $ mkdir plugins
    $ git clone git@bitbucket.org:yitsushi/shrimp-sh_user.git plugins/sh_user

**Or if you want to use with git and include Shrimp as a submodule**

    $ cd path/to/your/webroot
    $ mkdir lib && touch lib/.gitkeep
    $ git submodule add git@bitbucket.org:yitsushi/shrimp.git lib/shrimp
    $ cp lib/shrimp/defaults/_htaccess .htaccess
    $ cp lib/shrimp/defaults/index.php index.php
    $ cp lib/shrimp/defaults/static/shrimp.png shrimp.png
    $ mkdir cache
    $ chmod 777 cache
    $ mkdir plugins && touch plugins/.gitkeep
    $ git add .
    $ git commit -m 'Initial commit'

**To add sh_user plugin to your git controller site**

    $ git submodule add git@bitbucket.org:yitsushi/shrimp-sh_user.git plugins/sh_user

Open **http://your.domain.tld/user/123.html** or **http://your.domain.tld/user/123.json** =)

default .htaccess file
----------------------

    Options +FollowSymLinks
    RewriteEngine On

    DirectorySlash Off

    rewritecond %{http_host} ^shrimp-project.com
    rewriteRule ^(.*) http://www.shrimp-project.com/$1 [R=301,L]

    RewriteBase /

    # shrimp-project.com (production)
    RewriteCond %{HTTP_HOST} ^www\.shrimp-project\.com [NC]
    RewriteRule ^upload/users/(.*)/([^/\.]*)(.jpeg)?$ http://static.shrimp-project.com/cache/$1_$2.jpeg [L]
    RewriteCond %{HTTP_HOST} ^www\.shrimp-project\.com [NC]
    RewriteRule ^upload/(.+) http://static.shrimp-project.com/upload/$1 [L]
    RewriteCond %{HTTP_HOST} ^www\.shrimp-project\.com [NC]
    RewriteRule ^cache/(.+) http://static.shrimp-project.com/cache/$1 [L]

    # shrimp-project.local (la-muse)
    RewriteCond %{HTTP_HOST} ^www\.shrimp-project\.local [NC]
    RewriteRule ^upload/users/(.*)/([^/\.]*)(.jpeg)?$ http://static.shrimp-project.local/cache/$1_$2.jpeg [L]
    RewriteCond %{HTTP_HOST} ^www\.shrimp-project\.local [NC]
    RewriteRule ^upload/(.+) http://static.shrimp-project.local/upload/$1 [L]
    RewriteCond %{HTTP_HOST} ^www\.shrimp-project\.local [NC]
    RewriteRule ^cache/(.+) http://static.shrimp-project.local/cache/$1 [L]

    RewriteCond %{REQUEST_FILENAME} !-f
    RewriteCond %{REQUEST_FILENAME} !-d
    RewriteCond %{REQUEST_URI} !^/cache.*$
    RewriteCond %{REQUEST_URI} !^/themes.*$
    RewriteRule ^(.*)$ index.php [L]

    # PHP env
    SetEnvIf HOST ^www\.shrimp-project\.com PHP_ENV=production
    SetEnvIf HOST ^www\.shrimp-project\.local PHP_ENV=development

default index.php
-----------------

    define('DEBUG',    true);

    if (DEBUG) {
      error_reporting(-1);
      ini_set('display_errors', '1');
    }

    define('DS', DIRECTORY_SEPARATOR);

    define('ROOT',    dirname(__FILE__));

    require_once(ROOT . DS . "lib" . DS . "shrimp" . DS . "bootstrap.php");
