<?php
function microtime_float() {
  list($usec, $sec) = explode(" ", microtime());
  return ((float)$usec + (float)$sec);
}

define('TIME_START', microtime_float());
define('SHRIMP_INCLUDE_PATH', dirname(__FILE__));

if (!defined('DS')) {
  define('DS', DIRECTORY_SEPARATOR);
}

if (!defined('CR')) {
  define("CR", "\n");
}

if (!defined('_DEFAULTS')) {
  define("_DEFAULTS", SHRIMP_INCLUDE_PATH . DS . "defaults");
}

if (!defined('PHP_ENV')) {
  if (is_array($_SERVER) && array_key_exists('PHP_ENV', $_SERVER)) {
    define('PHP_ENV', $_SERVER['PHP_ENV']);
  } else if (is_array($_ENV) && array_key_exists('PHP_ENV', $_ENV)) {
    define('PHP_ENV', $_ENV['PHP_ENV']);
  } else {
    define('PHP_ENV', 'development');
  }
}

if (!defined('ROOT')) {
  define('ROOT', dirname(dirname(SHRIMP_INCLUDE_PATH)));
}

if (!defined('APP')) {
  define('APP', ROOT . DS . 'app');
}

// Own libraries
if (!defined('OLIB')) {
  define('OLIB', ROOT . DS . 'lib');
}

if (!defined('ETC')) {
  define('ETC', ROOT . DS . 'etc');
}

if (!defined('APP_LIB')) {
  define('APP_LIB', ROOT . DS . 'lib');
}

if (!defined('CACHE')) {
  define('CACHE', ROOT . DS . 'cache');
}

if (!defined('WEBSTATIC')) {
  define('WEBSTATIC', ROOT . DS . 'static');
}

if (!defined('CSS')) {
  define('CSS', WEBSTATIC . DS . 'css');
}

if (!defined('JS')) {
  define('JS', WEBSTATIC . DS . 'js');
}

if (!defined('IMAGES')) {
  define('IMAGES', WEBSTATIC . DS . 'images');
}

if (!defined('PLUGINS')) {
  define('PLUGINS', ROOT . DS . 'plugins');
}

require_once(SHRIMP_INCLUDE_PATH . DS . 'base.php');
require_once(SHRIMP_INCLUDE_PATH . DS . 'core' . DS . 'app.php');
require_once(SHRIMP_INCLUDE_PATH . DS . 'utils' . DS . 'string_util.php' );

spl_autoload_register(array('App', 'load'));

App::register('Config',       'config');
App::register('ConfigCache',  'cache_fs');
App::register('Object',       'core');
App::register('Request',      'net');
App::register('Router',       'routing');
App::register('Debug',        'debug');
App::register('Controller',   'core');
App::register('Shrimp\Database\Dao',          'database');
App::register('PluginStore',  'plugin');
App::register('Smarty',       'template');

PluginStore::trace_plugins();

if (file_exists(APP . DS . "boot.php")) {
  require_once APP . DS . "boot.php";
}

Router::match();
define('TIME_FINISH', microtime_float());

if (DEBUG) {
  if (Request::extension() === 'json') {
    echo "/* " , (TIME_FINISH - TIME_START), " seconds */\n";
  } elseif (Request::extension() === 'html') {
    echo "<!-- " , (TIME_FINISH - TIME_START), " seconds -->\n";
  }
}
