<?php

class App {
  protected static $__classes = array();

  public static function load($name) {
    if (!isset(self::$__classes[StringUtil::underscore($name)])) {
      return false;
    }
    
    $name = StringUtil::underscore($name, true);

    if (is_file(SHRIMP_INCLUDE_PATH . DS . self::$__classes[$name] . DS . $name . ".php")) {
      require_once(SHRIMP_INCLUDE_PATH . DS . self::$__classes[$name] . DS . $name . ".php");
    } elseif (is_file(self::$__classes[$name] . DS . $name . ".php")) {
      require_once(self::$__classes[$name] . DS . $name . ".php");
    } elseif (is_file(OLIB . DS . self::$__classes[$name] . DS . $name . ".php")) {
      require_once(OLIB . DS . self::$__classes[$name] . DS . $name . ".php");
    } else {
      throw new Exception(self::$__classes[$name] . DS . $name . ".php | Not found");
    }
  }
  
  public static function register($class, $path) {
    return (self::$__classes[StringUtil::underscore($class)] = $path);
  }
  
  public static function is_registered($class) {
    return array_key_exists(StringUtil::underscore($class), self::$__classes);
  }
  
  public static function add_lib($class, $path) {
    return (self::$__classes[StringUtil::underscore($class, true)] = $path);
  }
}