<?php
class Object {
  public function __construct() {
  }

  public function toString() {
    $class = get_class($this);
    return $class;
  }

  public function &__get($name) {
    if (property_exists($this, $name)) {
      return $this->{$name};
    } else {
      return null;
    }
  }

  public function __set($name, $value) {
    $this->{$name} = $value;
  }

  public function mget(array $array, $as_key_value = false) {
    $return_value = array();
    foreach($array as $name) {
      if (property_exists($this, $name)) {
        $value = $this->{$name};
      } else {
        $value = null;
      }
      if ($as_key_value === true) {
        $return_value[$name] = $value;
      } else {
        array_push($return_value, $value);
      }
    }
    return $return_value;
  }
  
  public function mset(array $array, $as_key_value = false) {
    foreach($array as $key => $value) {
      $this->{$key} = $value;
    }
    return true;
  }
}