<?php
class Controller {
  protected $this_is_plugin       = false;
  protected $plugin_name          = null;
  protected $original_controller  = null;
  protected $page                 = null;

  public function __construct() {
    if (!is_dir(CACHE . '/templates')) {
      mkdir(CACHE . '/templates');
    }
    $this->page = new Smarty();
    $this->page->compile_dir = CACHE . '/templates';

  }

  protected function render($parameters, $layout = 'default') {
    $mod = StringUtil::underscore(get_called_class());

    if (is_array($parameters)) {
      var_dump("TODO: separated_by_format");
    } else {
      $this->render_file($mod, $parameters, $layout, Request::extension());
    }
  }

  protected function render_file($mod, $name, $layout = 'default', $format = 'html') {
    if ($format === 'json') {
      header("Content-type: application/json");
    }
    $_layout = APP . DS . "view" . DS . "{$layout}.{$format}";
    if (!is_file($_layout) && $this->plugin_name !== null) {
      $_layout = PLUGINS . DS . $this->plugin_name . "view" . DS . "{$layout}.{$format}";
    }
    if (!is_file($_layout)) {
      $_layout = _DEFAULTS . DS . "view" . DS . "{$layout}.{$format}";
    }

    if (!is_file($_layout)) {
      die('404');
    }
    
    $this->page->template_dir = dirname($_layout);

    if ($this->this_is_plugin) {
      $ending = DS . 'view' . DS . $mod . DS . "{$name}.{$format}";

      if ( $this->original_controller !== null && is_file(APP . $ending)) {
        $this->page->assign('_main_view', APP . $ending);
        $this->page->display($_layout);
      } elseif ( $this->plugin_name !== null && is_file(PLUGINS . DS . $this->plugin_name . $ending)) {
        $this->page->assign('_main_view', PLUGINS . DS . $this->plugin_name . $ending);
        $this->page->display($_layout);
      } else {
        throw new Exception('Fatal error with called view');
      }
    } else {
      $ending = DS . 'view' . DS . $mod . DS . "{$name}.{$format}";

      if ( is_file(APP . $ending)) {
        $this->page->assign('_main_view', APP . $ending);
      } else {
        // TODO: what can I do if view does not exist
        // throw new Exception('Fatal error with called view');
      }

      $this->page->display($_layout);
    }
  }
}
