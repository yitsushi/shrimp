<?php

abstract class ConfigCache {
  private static $cache_file = null;
  const TTL = DAY;

  public static function write($data) {
    self::check_and_create();
    
    file_put_contents(self::$cache_file, serialize($data));
  }
  
  public static function read() {
    self::check_and_create();
    if (is_file(self::$cache_file)) {
      return @unserialize(file_get_contents(self::$cache_file));
    } else {
      return false;
    }
  }
  
  private static function check_and_create() {
    self::$cache_file = CACHE . DS . "etc" . DS . PHP_ENV . ".cache";

    if (!is_dir(dirname(self::$cache_file))) {
      mkdir(dirname(self::$cache_file));
    }

    if (is_file(self::$cache_file)) {
      $mtime = filemtime(self::$cache_file);
      if (is_file(ETC . DS . "_default.json")) {
        $default_mtime = filemtime(ETC . DS . "_default.json");
      } else {
        $default_mtime = filemtime(_DEFAULTS . DS . "etc" . DS . "default.json");
      }
      
      if (is_file(ETC . DS . PHP_ENV . ".json")) {
        $env_mtime = filemtime(ETC . DS . PHP_ENV . ".json");
      } else {
        $env_mtime = 0;
      }
      
      if (((time() - $mtime) > self::TTL) || ($default_mtime > $mtime) || ($env_mtime > $mtime)) {
        unlink(self::$cache_file);
      }
    }
  }
}