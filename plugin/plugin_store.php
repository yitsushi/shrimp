<?php
class PluginStore {
  protected static $__plugins = array();
  
  public static function trace_plugins() {
    if (!is_dir(PLUGINS)) return false;
    $plugin_dir = opendir(PLUGINS);
    while (($plugin = readdir($plugin_dir)) !== false) {
      if (in_array($plugin, array('.', '..'))) {
        continue;
      }
      if (is_file(PLUGINS . DS . $plugin . DS . 'boot.php')) {
        require_once(PLUGINS . DS . $plugin . DS . 'boot.php');
      }
    }
  }
  
  public static function register($name, $class, $path) {
    $path = PLUGINS . DS . $name . DS . $path;
    if (!in_array($name, self::$__plugins)) self::$__plugins[$name] = array();
    array_push(self::$__plugins[$name], $class);
    App::register($class, $path);
  }
}