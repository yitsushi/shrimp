<?php
abstract class Debug {
  protected static $level = 0;
  public static function start($marker = 'D') {
    echo self::get_indent_spaces() . "<pre>--{$marker}-", CR;
    ob_start();
    self::$level += 1;
  }
  
  public static function end($marker = 'D') {
    $content = ob_get_contents();
    ob_end_clean();
    $content = preg_replace("/^/m", self::get_indent_spaces(), $content);
    // if (substr($content, strlen($content)-2) !== "\n") {
    if (!preg_match("/\n$/", $content)) {
      $content .= "\n";
    }
    self::$level -= 1;
    if (DEBUG) {
      echo $content;
      echo self::get_indent_spaces() . "-/{$marker}-</pre>", CR;
    }
  }
  
  private static function get_indent_spaces() {
    $indent = '';
    for($i = 0; $i < self::$level; $i++) {
      $indent .= '  ';
    }
    return $indent;
  }
}