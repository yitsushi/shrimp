<?php
abstract class Config {
  private static $table = null;
  
  public static function get($name) {
    self::check_and_create($name);
    if (array_key_exists($name, self::$table)) {
      return self::$table[$name];
    } else {
      return null;
    }
  }
  
  public static function set($name, $value) {
    self::check_and_create($name);
    return self::$table[$name] = $value;
  }
  
  private static function check_and_create($name) {
    if (self::$table !== null) return true;
    
    $data = ConfigCache::read();
    if ($data) {
      self::$table = $data;
      return true;
    }
    
    if (!file_exists(ETC . DS . '_default.json')) {
      if (!file_exists(_DEFAULTS . DS . "etc" . DS . "default.json")) {
        throw new Exception('`' . _DEFAULTS . DS . "etc" . DS . 'default.json` configuration file not found!');
      } else {
        $default = json_decode(file_get_contents(_DEFAULTS . DS . "etc" . DS . "default.json"));
      }
    } else {
      $default = json_decode(file_get_contents(ETC . DS . '_default.json'));
    }
    if (file_exists(ETC . DS . PHP_ENV . '.json')) {
      $current = json_decode(file_get_contents(ETC . DS . PHP_ENV . '.json'));
      // throw new Exception('`' . ETC . DS . PHP_ENV . '.json` configuration file not found!');
    } else {
      $current = new stdClass;
    }
    
    $table = array_merge(object_to_array($default), object_to_array($current));

    self::generate_cached_table($table);
    ConfigCache::write(self::$table);
  }
  
  private static function generate_cached_table($array, $base = '') {
    foreach($array as $key => $value) {
      if(is_array($value)) {
        self::generate_cached_table($value, "{$base}{$key}.");
      } else {
        self::$table["{$base}{$key}"] = $value;
      }
    }
  }
}