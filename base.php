<?php

define('SECOND',  1);
define('MINUTE',  60);
define('HOUR',    3600);
define('DAY',     86400);
define('WEEK',    604800);
define('MONTH',   2592000);
define('YEAR',    31536000);

function file_exists_in_include_path($filepath) {
  $paths = explode(PATH_SEPARATOR, ini_get('include_path'));
  
  foreach($paths as $path) {
    $absolute = $path . DS . $filepath;
    if(file_exists($absolute)) {
      return $absolute;
    } else if(file_exists($filepath)) {
      return $filepath;
    }
    
    return false;
  }
}

function object_to_array($obj) {
  $arr = array();
  $arrObj = is_object($obj) ? get_object_vars($obj) : $obj;
  foreach ($arrObj as $key => $val) {
    $val = (is_array($val) || is_object($val)) ? object_to_array($val) : $val;
    $arr[$key] = $val;
  }
  return $arr;
}
