<?php
class StringUtil {
  protected static $__underscore_cache = array();
  protected static $__camelize_cache   = array();
  protected static $_transliteration   = array(
    '/ä|æ|ǽ/' => 'ae',
    '/œ/' => 'oe',
    '/Ä/' => 'Ae',
    '/À|Á|Â|Ã|Ä|Å|Ǻ|Ā|Ă|Ą|Ǎ/' => 'A',
    '/à|á|â|ã|å|ǻ|ā|ă|ą|ǎ|ª/' => 'a',
    '/Ç|Ć|Ĉ|Ċ|Č/' => 'C',
    '/ç|ć|ĉ|ċ|č/' => 'c',
    '/Ð|Ď|Đ/' => 'D',
    '/ð|ď|đ/' => 'd',
    '/È|É|Ê|Ë|Ē|Ĕ|Ė|Ę|Ě/' => 'E',
    '/è|é|ê|ë|ē|ĕ|ė|ę|ě/' => 'e',
    '/Ĝ|Ğ|Ġ|Ģ/' => 'G',
    '/ĝ|ğ|ġ|ģ/' => 'g',
    '/Ĥ|Ħ/' => 'H',
    '/ĥ|ħ/' => 'h',
    '/Ì|Í|Î|Ï|Ĩ|Ī|Ĭ|Ǐ|Į|İ/' => 'I',
    '/ì|í|î|ï|ĩ|ī|ĭ|ǐ|į|ı/' => 'i',
    '/Ĵ/' => 'J',
    '/ĵ/' => 'j',
    '/Ķ/' => 'K',
    '/ķ/' => 'k',
    '/Ĺ|Ļ|Ľ|Ŀ|Ł/' => 'L',
    '/ĺ|ļ|ľ|ŀ|ł/' => 'l',
    '/Ñ|Ń|Ņ|Ň/' => 'N',
    '/ñ|ń|ņ|ň|ŉ/' => 'n',
    '/Ö|Ò|Ó|Ô|Õ|Ō|Ŏ|Ǒ|Ő|Ơ|Ø|Ǿ/' => 'O',
    '/ö|ò|ó|ô|õ|ō|ŏ|ǒ|ő|ơ|ø|ǿ|º/' => 'o',
    '/Ŕ|Ŗ|Ř/' => 'R',
    '/ŕ|ŗ|ř/' => 'r',
    '/Ś|Ŝ|Ş|Š/' => 'S',
    '/ś|ŝ|ş|š|ſ/' => 's',
    '/Ţ|Ť|Ŧ/' => 'T',
    '/ţ|ť|ŧ/' => 't',
    '/Ü|Ù|Ú|Û|Ũ|Ū|Ŭ|Ů|Ű|Ų|Ư|Ǔ|Ǖ|Ǘ|Ǚ|Ǜ/' => 'U',
    '/ü|ù|ú|û|ũ|ū|ŭ|ů|ű|ų|ư|ǔ|ǖ|ǘ|ǚ|ǜ/' => 'u',
    '/Ý|Ÿ|Ŷ/' => 'Y',
    '/ý|ÿ|ŷ/' => 'y',
    '/Ŵ/' => 'W',
    '/ŵ/' => 'w',
    '/Ź|Ż|Ž/' => 'Z',
    '/ź|ż|ž/' => 'z',
    '/Æ|Ǽ/' => 'AE',
    '/ß/' => 'ss',
    '/Ĳ/' => 'IJ',
    '/ĳ/' => 'ij',
    '/Œ/' => 'OE',
    '/ƒ/' => 'f'
  );
  
  public static function slug($string, $lowercase = false, $substitute = '-') {
    $q_subtitute = preg_quote($substitute, '/');
    
    $merge = array(
      '/[^\s\p{Ll}\p{Lm}\p{Lo}\p{Lt}\p{Lu}\p{Nd}]/mu' => ' ',
      '/\\s+/' => $substitute,
      sprintf('/^[%s]+|[%s]+$/', $q_subtitute, $q_subtitute) => '',
    );
    
    $replace_map = array_merge($merge, self::$_transliteration);
    $slug = preg_replace(
      sprintf("/[%s]+/", $q_subtitute),
      $substitute,
      preg_replace(
        array_keys($replace_map),
        array_values($replace_map),
        $string
      )
    );
    
    if ($lowercase) {
      $slug = strtolower($slug);
    }
    
    return $slug;
  }
  
  public static function underscore($string, $named = true) {
    if(array_key_exists($string, self::$__underscore_cache)) {
      return self::$__underscore_cache[$string];
    }
    $name = $string;
    if ($named === true) {
      $name = preg_replace("/^.*\\\\/", '', $name);
    }
    $name = preg_replace("/[^a-zA-Z]/", "", $name);
    preg_match_all("/([A-Z][a-z]+)/", $name, $result);
    if (count($result)) {
      $m_string = '';
      if(count($result[0]) > 0) {
        $name = implode("_", array_map("strtolower", $result[0]));
      }
    }
    self::$__underscore_cache[$string] = $name;
    return $name;
  }
  
  public static function camelize($string) {
    if(array_key_exists($string, self::$__camelize_cache)) {
      return self::$__camelize_cache[$string];
    }
    
    $name = strtolower($string);
    preg_match_all("/([a-z]+)/", $name, $result);
    $name = preg_replace("/_/", "", $name);
    if (count($result)) {
      $m_string = '';
      if(count($result[0]) > 0) {
        $name = implode("", array_map("ucfirst", $result[0]));
      }
    }
    self::$__camelize_cache[$string] = $name;
    return $name;
  }
}