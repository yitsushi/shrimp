<?php
abstract class Request {
  // Get any parameters via _GET or _POST...
  // First _POST and if the string in parameter is not fount search in _GET...
  // If nothing then return $default (if not defined: null)
  public static function get($name, $type = '', $default = null) {
    if (array_key_exists($name, $_POST) && ($type == '' || $type == 'post' )) { return self::protect($_POST[$name]); }
    if (array_key_exists($name, $_GET) && ($type == '' || $type == 'get' )) { return self::protect($_GET[$name]); }
    return $default;
  }

  public static function test($name, $need, $type = '') {
    return (self::get($name, $type) === $need);
  }

  // Get any parameters via _SERVER
  // If nothing then return NULL
  public static function server($name) {
    if (array_key_exists($name, $_SERVER)) { return $_SERVER[$name]; }
    return null;
  }

  // Get the current controller...
  // If controller is not found in request then return default controller
  public static function controller() {
    if (array_key_exists('controller', $_SERVER) && $_SERVER['controller'] != '') {
      return preg_replace('/[;&#]/', '', $_SERVER['controller']);
    }
    return '|static|';
  }
  
  // Get the current action...
  // If action is not found in request then return default action
  public static function action() {
    if (array_key_exists('action', $_SERVER) && $_SERVER['action'] != '') {
      return preg_replace('/[;&#]/', '', $_SERVER['action']);
    }
    return 'index';
  }

  // Get the current path...
  public static function path() {
    return "/" . self::server('REQUEST_FULL_PATH');
  }
  
  public static function method() {
    return strtoupper(self::server('REQUEST_METHOD'));
  }

  // If get any data from _GET or _POST then this function will be protect for mysql and html...
  public static function protect($value) {
    if (is_array($value)) {
      foreach($value as $key => $v) {
        $value[$key] = self::protect($value[$key]);
      }
    } else {
      // $value = mysql_real_escape_string($value);
      $value = htmlspecialchars($value);
    }

    return $value;
  }

  public static function extension($value = null) {
    if($value == null) return self::server('FILE_EXTENSION');

    $_SERVER['FILE_EXTENSION'] = $value;
    return $value;
  }
  
  public static function _parse() {
    $_SERVER['FILE_EXTENSION']  = 'html';
    $query = self::server('REQUEST_URI');

    if (preg_match('/^\/index\.php/',$query)) {
      return true;
    }

    foreach($_GET as $k => $v) {
      $v = basename($v);
      if (in_array($k, array('controller'))) {
        $_SERVER['controller'] = $v;
      } elseif (in_array($k, array('action'))) {
        $_SERVER['action'] = $v;
      }
      unset($_GET[$k]);
    }

    $temp_query_str = preg_split('/\?/', $query);
    $query = $temp_query_str[0];
    $parameters = (count($temp_query_str) > 1) ? preg_split('/\&/', $temp_query_str[1]) : array();

    foreach($parameters as $p) {
      if (strpos($p, '=') > 0) {
        list($key, $value) = preg_split('/=/', $p, 2);
        $_GET[$key] = $value;
      } else {
        $_GET[$p] = true;
      }
    }

    $query = preg_replace('/(^\/)|(\/$)/', '', $query);
    if (!$query) return false;

    if(preg_match("/(.*)\.([a-z]+)$/", $query, $matches)) {
      list($path, $query, $ext) = $matches;
    } else {
      $ext = "html";
      $path = "{$query}.{$ext}";
    }

    $_SERVER['FILE_EXTENSION']     = $ext;
    $_SERVER['REQUEST_FULL_PATH']  = $path;
    $_SERVER['REQUEST_FULL_QUERY'] = $query;

    $tmp = preg_split('/\//', $query);
    if (count($tmp) < 1) return false;

    if (count($tmp) > 0) $_SERVER['controller'] = array_shift($tmp);
    if (count($tmp) < 1) return false;

    foreach($tmp as $i => $param) {
      $_GET[$i+1] = $param;
      if (preg_match('/\=/',$param)) {
        $tmp = preg_split('/\=/', $param);
        if(!array_key_exists($tmp[0], $_GET)) {
          $_GET[$tmp[0]] = $tmp[1];
        }
      }
    }
  }
}
Request::_parse();