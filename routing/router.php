<?php
// TODO: need cache
// TODO: add controller selection
abstract class Router {
  private static $routes = array();
  private static $bindings = array();

  private static $regexp = array(
    ':controller'   => "(?<controller>[a-z_]+)",
    ':id'           => "(?<id>[0-9]+)",
    ':format'       => "\.?(?<format>[a-z]+)$"
  );

  public static function bind($alias, $controller) {
    return (self::$bindings[$alias] = $controller);
  }

  public static function get($path, $action, $controller = null) {
    self::add_route('GET', $path, $action, $controller);
  }

  public static function post($path, $action, $controller = null) {
    self::add_route('POST', $path, $action, $controller);
  }

  public static function put($path, $action, $controller = null) {
    self::add_route('PUT', $path, $action, $controller);
  }

  public static function DELETE($path, $action, $controller = null) {
    self::add_route('DELETE', $path, $action, $controller);
  }

  public static function add_route($method, $path, $action, $controller = null) {
    $new_route = array(
      'method'  => strtoupper($method),
      'path'    => $path,
      'action'  => $action
    );
    if ($controller !== null) $new_route['controller'] = $controller;
    return array_push(
      self::$routes,
      $new_route
    );
  }

  private static $default_routes = array(
    array( // display a list of all :controller entries
      'method'  => 'GET',
      'path'    => '/:controller:format',
      'action'  => 'index'),
    array( // return an HTML form for creating a new :controller entry
      'method'  => 'GET',
      'path'    => '/:controller/new:format',
      'action'  => 'create'),
    array( // create a new :controller entry
      'method'  => 'POST',
      'path'    => '/:controller:format',
      'action'  => 'save_new'),
    array( // display a specific :controller entry
      'method'  => 'GET',
      'path'    => '/:controller/:id:format',
      'action'  => 'show'),
    array( // return an HTML form for editing a :controller entry
      'method'  => 'GET',
      'path'    => '/:controller/:id/edit:format',
      'action'  => 'edit'),
    array( // update a specific :controller entry
      'method'  => 'PUT',
      'path'    => '/:controller/:id:format',
      'action'  => 'update'),
    array( // delete a specific :controller entry
      'method'  => 'DELETE',
      'path'    => '/:controller/:id:format',
      'action'  => 'destroy')
  );

  private static function generate_regexp_path($path) {
    $path = preg_replace('/\//', '\\/', $path);
    return "/^" . preg_replace(array_map(function($key) { return "/{$key}/"; }, array_keys(self::$regexp)),
                               array_values(self::$regexp),
                               $path
                              ) . "$/";
  }

  private static function route_match_on_path($route) {
    if (strtoupper($route['method']) !== Request::method()) {
      return false;
    }
    $variables = array();
    $path = self::generate_regexp_path($route['path']);

    if (preg_match($path, Request::path(), $matches)) {

      $i = 0;
      foreach($matches as $k => $v) {
        if ($i % 2 === 1) {
          $variables[$k] = $v;
        }
        $i++;
      }
      return array(
        'match'     => $matches[0],
        'variables' => $variables,
        'route'     => $route
      );
    }
    return false;
  }

  public static function match() {
    $_ = false;
    foreach(self::$routes as $route) {
      $_ = self::route_match_on_path($route);
      if ($_ !== false) break;
    }
    if ($_ === false) {
      foreach(self::$default_routes as $route) {
        $_ = self::route_match_on_path($route);
        if ($_ !== false) break;
      }
    }

    if ($_ !== false) {
      if (!array_key_exists('format', $_['variables'])) {
        $_['variables']['format'] = Request::extension();
      } else {
        Request::extension($_['variables']['format']);
      }
      if (array_key_exists('controller', $_['variables'])) {
        $_SERVER['controller'] = $_['variables']['controller'];
      }
      if (array_key_exists('controller', $_['route'])) {
        $_SERVER['controller'] = $_['route']['controller'];
      }
      $_SERVER['action'] = $_['route']['action'];

      if (array_key_exists(Request::controller(), self::$bindings)) {
        $_SERVER['controller'] = self::$bindings[Request::controller()];
      }
      return self::route_handler($_);
    }

    return self::route_handler(array(
      'match'     => '/',
      'variables' => array(),
      'route'     =>  array(
        'method'  => Request::method(),
        'path'    => '/',
        'action'  => 'index'
      )
    ));
  }

  private static function route_handler($selected_route) {
    /*
    Debug::start();
    echo "Variables:", CR;
    foreach($selected_route['variables'] as $key => $value) {
      echo "  \${$key} = {$value}", CR;
    }
    Debug::end();
    */
    if (Request::controller() == '|static|') {
      if (file_exists(WEBSTATIC . DS . Request::action() . "." . Request::extension())) {
        return require(WEBSTATIC . DS . Request::action() . "." . Request::extension());
      }
    } else {
      if (is_file(APP . DS . 'controller' . DS . Request::controller() . '.php')) {
        require_once(APP . DS . 'controller' . DS . Request::controller() . '.php');
        $_c = StringUtil::camelize(Request::controller());
      } elseif (App::is_registered(Request::controller())) {
        $_c = Request::controller();
      } else {
        return false;
      }

      $c = new $_c;
      unset($selected_route['variables']['action']);
      unset($selected_route['variables']['controller']);
      if (method_exists($c, Request::action()) && is_callable(array($c, Request::action()))) {
        return call_user_func_array(array($c, Request::action()), array_values($selected_route['variables']));
      }
    }

    $file = Request::server('REQUEST_FULL_PATH');
    if ($file === null) {
      $file = "index.html";
    } else {
      $file = str_replace("|static|/", "", urldecode($file));
    }

    if (is_file(_DEFAULTS . DS . "static" .DS . $file)) {
      return require(_DEFAULTS . DS . "static" .DS . $file);
    } elseif (is_file(_DEFAULTS . DS . "static" .DS . "error_404." . Request::extension())) {
      header("HTTP/1.0 404 Not Found");
      return require(_DEFAULTS . DS . "static" .DS . "error_404." . Request::extension());
    } else {
      header("HTTP/1.0 404 Not Found");
      die("404");
    }
  }
}
