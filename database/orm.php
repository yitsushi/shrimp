<?php
require_once(dirname(__FILE__) . DS . "idiorm.php");
if ( Config::get('database.mysql.host')
  && Config::get('database.mysql.name')
  && Config::get('database.mysql.username')
  && Config::get('database.mysql.password')
) {
  ORM::configure('mysql:host='.Config::get('database.mysql.host').';dbname='.Config::get('database.mysql.name'));
  ORM::configure('username', Config::get('database.mysql.username'));
  ORM::configure('password', Config::get('database.mysql.password'));
} else {
  die('Database config does not exists!');
}