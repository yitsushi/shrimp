<?php
namespace Shrimp\Database;
require_once(dirname(__FILE__) . DS . "i_dao.php");
require_once(dirname(__FILE__) . DS . "orm.php");

abstract class Dao implements IDao {
  final protected static function find_by_pk_builder($pk) {
    $original = get_called_class();
    return \ORM::for_table($original::table_name())
                ->where($original::table_name() . '.' . $original::pk(), $pk);
  }
  
  public static function find_by_pk($pk) {
    $object = self::find_by_pk_builder($pk);
    return $object->find_one();
  }
  
  public static function pk() {
    return "id";
  }
}