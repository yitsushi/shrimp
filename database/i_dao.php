<?php
namespace Shrimp\Database;

interface IDao {
  public static function table_name();
  public static function pk();
}